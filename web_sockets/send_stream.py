import io
import socket
import struct
import time
import picamera

def setup_socket_connection(ip_addr, port):
    # Connect a client socket to my_server:8000 (change my_server to the
    # hostname of your server)
    client_socket = socket.socket()
    client_socket.connect((ip_addr, port))

    # Make a file-like object out of the connection
    connection = client_socket.makefile('wb')
    return client_socket, connection

def boot_camera(height, width):
    camera = picamera.PiCamera()
    camera.resolution = (width, height)
    # Start a preview and let the camera warm up for 2 seconds
    camera.start_preview()
    time.sleep(2)
    return camera

class Timer():
    def __init__(self, t):
        # Simple timer, unit is seconds
        self.start = time.time()
        self.timeout = t

    def time_running(self):
        return time.time() - self.start < self.timeout

def send_camera_stream_to_connection(camera, connection, timer):
    stream = io.BytesIO()
    for foo in camera.capture_continuous(stream, 'jpeg'):
        # Write the length of the capture to the stream and flush to
        # ensure it actually gets sent
        connection.write(struct.pack('<L', stream.tell()))
        connection.flush()
        # Rewind the stream and send the image data over the wire
        stream.seek(0)
        connection.write(stream.read())
        # If we've been capturing for more than 30 seconds, quit
        if not timer.time_running():
            break
        # Reset the stream for the next capture
        stream.seek(0)
        stream.truncate()
    # Write a length of zero to the stream to signal we're done
    connection.write(struct.pack('<L', 0))

def main():
    print("Booting camera")
    camera = boot_camera(480, 640)
    ip_addr = '192.168.1.219'
    port = 8000
    print("Creating connection to: %s:%d" % (ip_addr, port))
    client_socket, connection = setup_socket_connection(ip_addr, port)
    print("Connected, sending stream...")
    timer = Timer(60)
    try:
        send_camera_stream_to_connection(
                camera,
                connection,
                timer
                )
    finally:
        print("Closing connection")
        connection.close()
        client_socket.close()
    print("Finished")

if __name__ == "__main__":
    main()
