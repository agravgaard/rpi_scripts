import io
import time
import socket
import struct
import cv2 as cv
import numpy as np

def create_socket(port):
    # Start a socket listening for connections on 0.0.0.0:8000 (0.0.0.0 means
    # all interfaces)
    server_socket = socket.socket()
    server_socket.bind(('0.0.0.0', 8000))
    server_socket.listen(0)
    return server_socket

def make_connection(server_socket):
    # Accept a single connection and make a file-like object out of it
    return server_socket.accept()[0].makefile('rb')

def make_video_out(filename):
    # Define the codec and create VideoWriter object
    fourcc = cv.VideoWriter_fourcc(*'XVID')
    return cv.VideoWriter(filename, fourcc, 1.5, (640,  480))

def raw_to_img(img_stream):
    # Construct a stream to hold the image data and read the image
    # data from the connection
    image_stream = io.BytesIO()
    image_stream.write(img_stream)
    # Rewind the stream, open it as an image with PIL and do some
    # processing on it
    image_stream.seek(0)
    file_bytes = np.asarray(bytearray(image_stream.read()), dtype=np.uint8)
    return cv.imdecode(file_bytes, cv.IMREAD_COLOR)


def video_from_img_stream(connection, filename):
    video_out = make_video_out(filename)
    while True:
        # Read the length of the image as a 32-bit unsigned int. If the
        # length is zero, quit the loop
        image_len = struct.unpack('<L', connection.read(struct.calcsize('<L')))[0]
        if not image_len:
            break
        img = raw_to_img(connection.read(image_len))
        video_out.write(img)
    video_out.release()

def main():
    print("Press q to quit")
    port = 8000
    clip_number = 0
    while True:
        print("Listening on port: %d" % port)
        server_socket = create_socket(port)
        connection = make_connection(server_socket)
        print("Made connection")

        filename = 'output_%d.avi' % clip_number

        print("Writing to %s" % filename)
        video_from_img_stream(connection, filename)

        print("Closing connection")
        connection.close()
        server_socket.close()

        time.sleep(3)
        clip_number += 1

    print("Finished")

if __name__ == "__main__":
    main()

