
import cv2 as cv
import numpy as np

try:
    from sense_hat import SenseHat
    sense = SenseHat()
    USE_SENSE_HAT=True
except:
    USE_SENSE_HAT=False

LOWLIGHT_THRESH = 63

if USE_SENSE_HAT:
    W = (255, 255, 255)
    R = (255, 0, 0)
    G = (0, 255, 0)
    B = (0, 0, 255)
    b = (0, 0, 127)
    K = (0, 0, 0)
    Y = (127, 127, 0)
    S = (205, 133, 63)
    Br = (102, 51, 0)
    
    steve_pixels = [
        Br, Br, Br, Br, Br, Br, Br, Br,
        Br, Br, Br, Br, Br, Br, Br, Br,
        Br, S,  S,  S,  S,  S,  S,  Br,
        S,  S,  S,  S,  S,  S,  S,  S,
        S,  W,  B,  S,  S,  B,  W,  S,
        S,  S,  S,  Br, Br, S,  S,  S,
        S,  S,  Br, S,  S,  Br, S,  S,
        S,  S,  Br, Br, Br, Br, S,  S
    ]
    
    def display_steve(sense):
        sense.clear()
        sense.set_pixels(steve_pixels)

class Detector():
    def __init__(self):
        self.face_casc = cv.CascadeClassifier("haarcascade_frontalface_default.xml")
        # self.eye_casc = cv.CascadeClassifier("haarcascade_eye.xml")

    def detect_face(self, framebuffer):
        framebytes = np.asarray(bytearray(framebuffer), dtype=np.uint8)
        img = cv.imdecode(framebytes, cv.IMREAD_COLOR)
        gray_img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
        if  np.mean(gray_img) < LOWLIGHT_THRESH:
            top = np.percentile(gray_img, 99)
            # Create mask: if < top: 255 else: 0
            _, img_mask = cv.threshold(
                    gray_img,
                    top,
                    255,
                    cv.THRESH_BINARY_INV
                    )
            # Normalize gray image to in mask
            gray_img = cv.normalize(
                    gray_img, gray_img,
                    0, 255,
                    cv.NORM_MINMAX,
                    mask=img_mask
                    )
            img = gray_img

        faces = self.face_casc.detectMultiScale(gray_img, 1.3, 5)

        if USE_SENSE_HAT:
            if len(faces) == 0:
                sense.clear()
            else:
                display_steve(sense)

        for (x,y,w,h) in faces:
            cv.rectangle(img, (x,y), (x+w, y+h), (255, 255, 0), 2)
            # roi_gray = gray_img[y:y+h], x:x+w]
            # roi_color = img[y:y+h], x:x+w]

            # # Detects eyes of different sizes in the input image
            # eyes = eye_casc.detectMultiScale(roi_gray)
    
            # # To draw a rectangle in eyes
            # for (ex,ey,ew,eh) in eyes:
            #     cv2.rectangle(roi_color,(ex,ey),(ex+ew,ey+eh),(0,127,255),2)
      
        _, framebuffer = cv.imencode('.JPEG', img)

        return framebuffer
