# This file has been written to your home directory for convenience. It is
# saved as "/home/pi/humidity-2019-07-16-18-18-10.py"

import time
from sense_hat import SenseHat

sense = SenseHat()

W = (255, 255, 255)
R = (255, 0, 0)
G = (0, 255, 0)
B = (0, 0, 255)
b = (0, 0, 127)
K = (0, 0, 0)
Y = (127, 127, 0)
S = (205, 133, 63)
Br = (102, 51, 0)

def display_steve(sense):
    sense.clear()
    steve_pixels = [
        Br, Br, Br, Br, Br, Br, Br, Br,
        Br, Br, Br, Br, Br, Br, Br, Br,
        Br, S,  S,  S,  S,  S,  S,  Br,
        S,  S,  S,  S,  S,  S,  S,  S,
        S,  W,  B,  S,  S,  B,  W,  S,
        S,  S,  S,  Br, Br, S,  S,  S,
        S,  S,  Br, S,  S,  Br, S,  S,
        S,  S,  Br, Br, Br, Br, S,  S
    ]
    sense.set_rotation(90)

def display_like(sense):
    sense.clear()
    like_pixels = [
        K, K, K, K, B, B, K, K, 
        K, K, K, K, B, B, K, K, 
        K, K, K, B, B, b, K, K, 
        B, B, B, B, B, B, B, B, 
        B, B, B, B, B, B, B, B, 
        B, B, B, B, B, B, B, b, 
        b, B, B, B, B, B, B, K, 
        B, B, K, K, K, K, K, K, 
    ]
    sense.set_pixels(like_pixels)
    sense.set_rotation(90)
    sense.flip_h()

sense.show_letter(
        "O",
        text_colour=G,
        back_colour=K
        )
time.sleep(1)

sense.show_letter(
        "X",
        text_colour=R,
        back_colour=K
        )
time.sleep(1)

class Timer():
    def __init__(self, t_seconds):
        self.timeout = t_seconds
        self.start = time.time()
    def is_time_left(self):
        return time.time() - self.start < self.timeout
    def restart(self):
        self.start = time.time()
    def set_new_time(t_seconds):
        self.timeout = t_seconds

timer = Timer(5)
while timer.is_time_left():
    humidity = sense.humidity
    humidity_value = 64 * humidity / 100
    pixels = [G if i < humidity_value else K for i in range(64)]
    sense.set_pixels(pixels)

sense.clear()

for x in range(8):
    for y in range(8):
        sense.set_pixel(x, y, Y)
        time.sleep(0.01)

timer.restart()
while timer.is_time_left():
    pressure = sense.get_pressure()
    sense.show_message(
            "%d" % round(pressure),
            text_colour=Y,
            scroll_speed=0.1
            )

timer.restart()
while timer.is_time_left():
    temperature = sense.get_temperature_from_humidity()
    sense.show_message(
            "%.1f C " % temperature,
            text_colour=G,
            scroll_speed=0.1
            )

timer.restart()
while timer.is_time_left():
    temperature = sense.get_temperature_from_pressure()
    sense.show_message(
            "%.1f C " % temperature,
            text_colour=Y,
            scroll_speed=0.1
            )

sense.clear()
